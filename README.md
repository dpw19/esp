# Módulos ESP para IoT

## Antecedentes
En la actualidad el Internet de las Cosas (IoT, por sus siglas en ingles) gana terreno y se impone la necesidad de conocer y acercarse a esta tecnología.

Esta nueva rama envuelve varios saberes tales como las bases de datos los protocolos de comunicación WiFi y los microcontroladores enfocados en TCP/IP.

Las tarjetas de desarrollo más usuales para tales fines son el ESP con sus diferentes familias y características.

Estos dispositivos en general que trabajan en el rango de los 3.3 volts y requieren mecanismos para su programación y adhesión en los proyectos que así lo requieren.

## Avances

En este repositorio se trabaja por aproximaciones en el diseño de tarjetas que permitan la integración de estos módulos.

* **Probado:** Diseño e implementación de interfase para programación y prueba del ESP01 con un módulo USB-TTL.
 * [usb-ttl-esp-programming-board](esp8266/esp01/usb-ttl-esp-programming-board)

* **En desarrollo:** Diseño e implementación módulo programador con el chip CH340G
 * [usb-ch340-esp-programming-board](esp8266/esp01/usb-ch340-esp-programming-board)

# KiCAD

Todos los diseños se realizan con el software libre [KiCAD](https://kicad.org) y se encuentran bajo la filosofía del Software/Harware Libres, es decir que los diseños cuentan con acceso a los esquemáticos PCB y Gerbers para producción.

# Licencias

Todos los materiales y diseños se encuentran bajo la Licencia GNU/GPL3 o posterior a menos que se especifique lo contrario.

Por ello se tiene la libertad de utilizar, estudiar, modificar, compartir los diseños siempre bajo la misma Licencia

Puede conocer la licencia GNU/GPL en el siguiente [enlace](https://www.gnu.org/licenses/gpl-3.0.html).

# Deslinde de responsabilidad

Todas los diseños se realizan con las mejores intenciones y prácticas para que sean funcionales. La información publicada en este sitio funcionó al autor y no por ello garantiza que le funcione o deba funcional a otras personas, por tal motivo se deslinda de cualquier daño directo o indirecto, incidental o subsecuentes a la vida y/o equipos por el uso de la información contenida en este repositorio.
