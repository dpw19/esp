# Programación Esp01 con módulo USB-TTL 

Es parte de la familia ESP8266. Solamente cuenta con algunos puertos GPIO y
se programa ayuda de un módulo USB/TTL.

Funciona con tan sólo 3V3.

En este repositorio se recopila información para programar la memoria flash de este modelo de ESP.

La [Wiki](https://commons.wikimedia.org/wiki/File:ESP8266_01_PinOut.png)
nos muestra la distribución de pines

![ESP01](https://upload.wikimedia.org/wikipedia/commons/0/08/ESP8266_01_PinOut.png)

## Programación

La primera cuestión para abordar es la programación de la memoria Flash de
este módulo.

Utilizaremos un módulo USB a TTL con las conexiones que propone [mischianti.com](https://www.mischianti.org/2019/01/14/esp-01-modules-programming-board/)

![Basic Programming Connection](https://www.mischianti.org/wp-content/uploads/2019/01/basicProgrammingConnection_bb.png )

## PCB

Se diseña y construye el PCB para interconectar el módulo USB-TTL a 3V3 y el ESP01.

Los archivos correspondientes se encuentran en las carpetas
* [KiCAD](KiCAD)
* [PDF](pdf)
* [GERBER](gerber)

![01_usb-ttl-esp-programming-board.png](img/01_usb-ttl-esp-programming-board.png)
![01_usb-ttl-esp-programming-board.png](img/02_usb-ttl-esp-programming-board.png)
![01_usb-ttl-esp-programming-board.png](img/03_usb-ttl-esp-programming-board.png)

## Preparación de la base de datos

Probaremos el proyecto de [randomnerdtutorials](https://randomnerdtutorials.com/control-esp32-esp8266-gpios-from-anywhere/) con un servidor propio.

El primer paso es configurar la base de datos. En este caso utilizamos tigertech.net

Una vez generada la base de datos se generan las tablas

```mysql
CREATE TABLE Outputs (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    board INT(6),
    gpio INT(6),
    state INT(6)
);
INSERT INTO `Outputs`(`name`, `board`, `gpio`, `state`) VALUES ("Built-in LED", 1, 2, 0);

CREATE TABLE Boards (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    board INT(6),
    last_request TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
INSERT INTO `Boards`(`board`) VALUES (1);
```

## Preparación del servidor

Clonamos los archivos del proyecto con:

```
git clone https://github.com/RuiSantosdotme/control-esp32-esp8266-gpios-from-anywhere.git
```
Nos pasamos al directorio Code

```
cd control-esp32-esp8266-gpios-from-anywhere/Code
```

Editamos el archivo esp-database.php para colocar los datos de la
base de datos en las variables

* $dbname
* $username
* $password

En este momento el servidor debería responder si vamos a la dirección

edupanti.org/IoT/esp-outputs.php

## Preparación del IDE Arduino para Esp01

Es necesario instalar la biblioteca Arduino_JSON, para ello

Herramientas --> Administrar bibliotecas -- Buscar la biblioteca Arduino-Arduino_JSON
